Included is the Final Project Report pdf that explains:
What the uploaded code files are, 
My initial project scope, 
What I did,
Something I ran into along the way,
and some things I learned.

The Code folder contains all of the files related to decompiling an Android apk and embedding malicious code into it via the ad library. (I haven't uploaded the apks I used because each one was several MBs each; I can always upload them to Google Drive or such and share them if wanted). 
